package com.sudoku;

import java.io.Console;
import java.io.IOException;
import java.util.Iterator;

/** Main runner for the Sudoku solver. */
public class Main {
  /**
   * Main program, expecting the only argument to be the path to the puzzle input text file.
   *
   * <p> The input file must have 9 lines, each of which is a space-delimited string. Each component
   * must be either a digit 1-9 or a non-digit character to represent unknown blank.
   *
   * <p> For example, the following is an example of a valid input file.
   *
   *     _ 1 9 _ 5 _ 3 2 _
   *     _ 8 5 _ _ _ _ _ _
   *     7 _ _ _ 9 _ _ _ 1
   *     4 _ _ 5 _ _ 9 _ _
   *     _ _ 2 _ 7 _ 5 _ _
   *     _ _ 8 _ _ 1 _ _ 3
   *     1 _ _ _ 4 _ _ _ 6
   *     _ _ _ _ _ _ 1 9 _
   *     _ 3 6 _ 2 _ 4 8 _
   */
  public static void main(String[] args) {
    if (args.length != 1) {
      System.err.println("usage: sudoku <input_file>");
      System.exit(1);
    }
    try {
      SudokuSolver solver = SudokuSolver.loadPuzzle(args[0]);
      Iterator<Solution> iter = solver.iterator();
      while (iter.hasNext()) {
        if (!promptForNext()) {
          System.out.println("Bye...");
          System.exit(0);
        }
        System.out.println("\n===== FOUND SOLUTION =====\n");
        iter.next().printToConsole();
      }
      System.out.println("===== ALL SOLUTIONS FOUND =====");
    } catch (IOException e) {
      System.err.println("Failed to load file: " + e.getMessage());
      System.exit(1);
    } catch (Throwable e) {
      // Any other initialization exceptions caught here.
      System.err.println("Failed to run solver: " + e.getMessage());
      System.exit(1);
    }

  }

  // Waits for user inputs from the console. "y" or ENTER means continue to show the next solution,
  // "n" or Ctrl-D means quit.
  private static boolean promptForNext() {
    Console console = System.console();
    // Loop until a valid user input.
    while (true) {
      String searchNext = console.readLine("Search for next solution (Y/n)? ");
      if (searchNext == null) {
        return false;
      }
      searchNext = searchNext.toLowerCase();
      if (searchNext.equals("n")) {
        return false;
      }
      if (searchNext.equals("y") || searchNext.isEmpty()) {
        return true;
      }
    }
  }
}
