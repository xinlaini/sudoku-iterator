package com.sudoku;

import java.util.HashSet;
import java.util.Set;

/**
 * Internal representation of a blank in the puzzle. It implements {@link Comparable} so
 * a natural ordering can be defined in such a way that the blank with fewer candidate digits comes
 * first. When two blanks have the same number of candidates, use their coordinates to break the
 * tie.
 */
class Blank implements Comparable<Blank> {
  // Coordinates in the 9x9 board.
  private final int x;
  private final int y;

  // The candidate digit this Blank is allowed to take, this variable changes during the iteration
  // of SudokuSolver.iterator().
  private HashSet<Integer> candidates;

  /**
   * Creates a {@link Blank} object, with its coordinate on the puzzle and the candidate digits for
   * its row, column and 3x3 block.
   *
   * @param x the 0-based x coordinate of the blank on the puzzle.
   * @param y the 0-based y coordinate of the blank on the puzzle.
   * @param rowCandidates the unused digits for the row this blank is on.
   * @param columnCandidates the unused digits for the column this blank is on.
   * @param blockCandidates the unused digits for the 3x3 block this blank is on.
   *
   * @throws {@link IllegalArgumentException} if the coordinate is out bound.
   */
  Blank(
      int x,
      int y,
      Set<Integer> rowCandidates,
      Set<Integer> columnCandidates,
      Set<Integer> blockCandidates) {
    if (x < 0 || x >= 9 || y < 0 || y >= 9) {
      throw new IllegalArgumentException("Invalid coordinate (" + x + ", " + y + ")");
    }
    this.x = x;
    this.y = y;
    // 3-way intersection determines the blank's candidate digits.
    candidates = new HashSet<>(rowCandidates);
    candidates.retainAll(columnCandidates);
    candidates.retainAll(blockCandidates);
  }

  int getX() {
    return x;
  }

  int getY() {
    return y;
  }

  int getBlockIndex() {
    return y / 3 * 3 + x / 3;
  }

  Set<Integer> getCandidates() {
    return new HashSet<Integer>(candidates);
  }

  boolean addCandidate(int digit) {
    return candidates.add(digit);
  }

  boolean removeCandidate(int digit) {
    return candidates.remove(digit);
  }

  @Override
  public int compareTo(Blank other) {
    // Candidate with smaller size yields a smaller blank.
    if (this.candidates.size() < other.candidates.size()) {
      return -1;
    }
    if (this.candidates.size() > other.candidates.size()) {
      return 1;
    }
    // Use coordinates to break ties.
    if (this.y < other.y) {
      return -1;
    }
    if (this.y > other.y) {
      return 1;
    }
    if (this.x < other.x) {
      return -1;
    }
    if (this.x > other.x) {
      return 1;
    }
    return 0;
  }
}
