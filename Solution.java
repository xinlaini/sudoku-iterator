package com.sudoku;

/** Immutable wrapper around {@link SudokuConfig} that represents a solution. */
class Solution {
  // The underlying solution config.
  private final SudokuConfig config;

  Solution(SudokuConfig config) {
    this.config = config;
  }

  void printToConsole() {
    config.printToConsole();
  }
}
