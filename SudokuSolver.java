package com.sudoku;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Sudoku solver class which loads the puzzle from input text file and iterates over all solutions.
 */
public class SudokuSolver implements Iterable<Solution> {
  // The original configuration representing the input puzzle.
  private final SudokuConfig originalPuzzle;

  /**
   * Creates the {@link SudokuSolver} from the given input text file.
   *
   * @param inputFile the path to the puzzle input text file.
   *
   * @throws IOException if {@code inputFile} cannot be loaded.
   *
   * @return the {@link SudokuSolver} object initially loaded with the puzzle.
   */
  public static SudokuSolver loadPuzzle(String inputFile) throws IOException {
    return new SudokuSolver(SudokuConfig.loadPuzzle(inputFile));
  }

  @Override
  public Iterator<Solution> iterator() {
    // Returns a new iterator working on a copy of the original puzzle.
    return new SolutionIterator(originalPuzzle.makeCopy());
  }

  /**
   * The actual implementation of the {@link Iterator} over solutions.
   *
   * <p> The iterator runs the main recursive solver function in a separate thread, but the progress
   * of that thread is synchronized with and controlled by the {@link #next()} calls.
   */
  private static class SolutionIterator implements Iterator<Solution> {
    // The scratch-data config containing the state of the iterator.
    private final SudokuConfig config;

    // Whether there is a next solution.
    private boolean hasNext = true;

    public SolutionIterator(SudokuConfig config) {
      this.config = config;
      // Starts the worker thread to run the recursive solver function.
      (new Thread(new Runnable() {
        @Override
        public void run() {
          // Invoke the recursive function entrance.
          try {
            solveRecursive(SolutionIterator.this.config.extractBlanks());
          } catch (Throwable e) {
            System.err.println("Invalid puzzle: " + e.getMessage());
            System.exit(1);
          }

          // Reached the end.
          synchronized(SolutionIterator.this) {
            SolutionIterator.this.hasNext = false;
            // This notifies the last next() call to unblock.
            SolutionIterator.this.notify();
          }
        }
      })).start();

      // Wait until we either got a solution, or the solver has ended.
      synchronized(this) {
        try {
          this.wait();
        } catch (InterruptedException e) {
          // Do nothing
        }
      }
    }

    @Override
    public synchronized boolean hasNext() {
      return hasNext;
    }

    @Override
    public synchronized Solution next() {
      // Guard against illegal use of iterator.
      if (!hasNext) {
        throw new IllegalStateException("Calling next() with hasNext() false");
      }
      // Safe to access config here because the solver thread should be blocked pending on the
      // subsequent notify() a few lines below.
      Solution solution = config.asSolution();

      // Notify the solver thread to continue searching for the next solution.
      this.notify();

      // Wait until the next solution is available or the iterator reaches the end.
      try {
        this.wait();
      } catch (InterruptedException e) {
        // Do nothing
      }

      return solution;
    }

    @Override
    public void remove() {
      throw new UnsupportedOperationException("not supported");
    }

    // Recursion entrance to run the solver for all the {@code remainingBlanks}.
    private void solveRecursive(List<Blank> remainingBlanks) {
      // No more remaining blanks to be solved, got one solution.
      if (remainingBlanks.isEmpty()) {
        synchronized(this) {
          // Notify that a solution has been obtained.
          hasNext = true;
          this.notify();

          // Wait until the iterator.next() signals OK for the next movement.
          try {
            this.wait();
          } catch (InterruptedException e) {
            // Do nothing
          }
        }
        return;
      }

      // Sort the remaining blanks by the size of their candidate digits (See Blank.compareTo()).
      Collections.sort(remainingBlanks);

      // The blank with the least candidate digits.
      Blank first = remainingBlanks.get(0);
      for (int digit : first.getCandidates()) {
        // Try this candidate digit.
        config.setValue(first.getX(), first.getY(), digit);

        ArrayList<Blank> nextRemainingBlanks = new ArrayList<>();
        // Subsets of remaining blanks which temporarily removed the digits for subsequent
        // recursion.
        HashSet<Blank> needRestore = new HashSet<>();

        for (int i = 1; i < remainingBlanks.size(); ++i) {
          Blank blank = remainingBlanks.get(i);
          // If a remaining blank is on the same row, column or 3x3 block as 'first', make sure
          // 'digit' is not used as a candidate for 'blank' for the subsequent recursion call.
          if ((first.getX() == blank.getX() || first.getY() == blank.getY()
               || (first.getBlockIndex() == blank.getBlockIndex()))
              && blank.removeCandidate(digit)) {
            needRestore.add(blank);
          }
          nextRemainingBlanks.add(blank);
        }

        // Invoke recursion on a smaller number of remaining blanks.
        solveRecursive(nextRemainingBlanks);

        // Now restore digit's candidacy for blank, if applicable.
        for (Blank blank : needRestore) {
          blank.addCandidate(digit);
        }
      }
    }
  }

  // Private constructor to be called from loadPuzzle() only.
  private SudokuSolver(SudokuConfig originalPuzzle) {
    System.out.println("===== LOADED PUZZLE =====\n");
    originalPuzzle.printToConsole();
    this.originalPuzzle = originalPuzzle;
  }
}
