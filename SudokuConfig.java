package com.sudoku;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Internal representation for a Sudoku board, which is potentially in a unsolved state.
 *
 * <p> This is to be owned by an {@link Iterator} over the solutions. It maintains the state of the
 * search for all solutions.
 */
class SudokuConfig {
  // For colorful printing of puzzle and solutions.
  private static final String ANSI_RESET = "\u001B[0m";
  private static final String ANSI_RED = "\u001B[31m";
  private static final String ANSI_GREEN = "\u001B[32m";
  private static final String ANSI_YELLOW = "\u001B[33m";

  // The 9x9 squares.
  private final Integer[][] value = new Integer[9][9];

  // Whether a square was originally set by the puzzle input.
  private final boolean[][] originallySet = new boolean[9][9];

  /**
   * Loads the {@link SudokuConfig} object from the given {@code inputFile}.
   *
   * @param inputFile the path to the puzzle input text file.
   *
   * @throws IOException if {@code inputFile} cannot be loaded.
   *
   * @return the {@link SudokuConfig} object initially loaded with the puzzle.
   */
  static SudokuConfig loadPuzzle(String inputFile) throws IOException {
    List<String> lines = Files.readAllLines(
        FileSystems.getDefault().getPath(inputFile),
        Charset.defaultCharset());

    // Verify input line count.
    if (lines.size() != 9) {
      throw new IllegalArgumentException(
          "Input file '" + inputFile + "' has " + lines.size() + " lines, expecting 9 lines.");
    }

    SudokuConfig config = new SudokuConfig();

    for (int y = 0; y < 9; ++y) {
      String[] parts = lines.get(y).split(" ");
      // Verify each line has 9 parts, delimited by spaces.
      if (parts.length != 9) {
        throw new IllegalArgumentException(
            "Line #" + (y + 1) + " has " + parts.length + " parts, expecting 9 parts.");
      }
      // Parse each component.
      for (int x = 0; x < 9; ++x) {
        try {
          config.value[y][x] = Integer.valueOf(parts[x]);
          config.originallySet[y][x] = true;
        } catch (NumberFormatException e) {
          // A NaN means blank.
          config.value[y][x] = null;
          config.originallySet[y][x] = false;
        }
      }
    }

    return config;
  }

  /**
   * Sets a value for a square.
   *
   * @param x the 0-based x coordinate of the square on the puzzle
   * @param y the 0-based y coordinate of the square on the puzzle
   * @param val the value for the square.
   */
  void setValue(int x, int y, int val) {
    // IndexOutOfBoundException will be implicitly thrown if (x,y) is out of bound.
    if (originallySet[y][x]) {
      throw new IllegalArgumentException(
          "Cannot mutate value for an originally set square (" + x + ", " + y + ")");
    }
    value[y][x] = val;
  }

  /**
   * Wraps the current config as a solution.
   */
  Solution asSolution() {
    // Make sure this is really a valid solution.
    verifySolution();

    // Creates a copy of the current data for the solution, so the future modification of 'this'
    // will not accidentally modify the Solution the caller keeps on hand.
    return new Solution(makeCopy());
  }

  /** Makes a copy of the current state */
  SudokuConfig makeCopy() {
    SudokuConfig copy = new SudokuConfig();
    for (int y = 0; y < 9; ++y) {
      for (int x = 0; x < 9; ++x) {
        copy.value[y][x] = this.value[y][x];
        copy.originallySet[y][x] = this.originallySet[y][x];
      }
    }
    return copy;
  }

  /** Prints colorful Sudoku puzzle or solution to the console. */
  void printToConsole() {
    StringBuilder builder = new StringBuilder();
    for (int y = 0; y < 9; ++y) {
      for (int x = 0; x < 9; ++x) {
        if (originallySet[y][x]) {
          // Digit is provided in the original puzzle, print in vanilla white.
          builder.append(value[y][x]);
        } else if (value[y][x] == null) {
          // Value not set, print a red '?'.
          builder.append(ANSI_RED + "?" + ANSI_RESET);
        } else {
          // Value is set, but not original, must be from a solution, print in green.
          builder.append(ANSI_GREEN + value[y][x] + ANSI_RESET);
        }

        // Digit separator or EOL.
        builder.append(x == 8 ? '\n' : ' ');

        // Vertical separator between 3x3 blocks for nicer viewing.
        if (x == 2 || x == 5) {
          builder.append(ANSI_YELLOW + "| " + ANSI_RESET);
        }
      }
      // Horizontal separator between 3x3 blocks for nicer viewing.
      if (y == 2 || y == 5) {
        builder.append(ANSI_YELLOW + "---------------------" + ANSI_RESET + "\n");
      }
    }
    System.out.println(builder.toString());
  }

  /** Extracts the {@link Blank}s from this config. */
  List<Blank> extractBlanks() {
    // Unused digits for each row.
    ArrayList<Set<Integer>> rowCandidates = new ArrayList<>(9);
    for (int y = 0; y < 9; ++y) {
      rowCandidates.add(extractUnused(value[y], "Row #" + y));
    }

    // Unused digits for each column.
    ArrayList<Set<Integer>> columnCandidates = new ArrayList<>(9);
    for (int x = 0; x < 9; ++x) {
      columnCandidates.add(extractUnused(getColumn(x), "Column #" + x));
    }

    // Unused digits for each 3x3 block.
    ArrayList<Set<Integer>> blockCandidates = new ArrayList<>(9);
    for (int by = 0; by < 3; ++by) {
      for (int bx = 0; bx < 3; ++bx) {
        blockCandidates.add(
            extractUnused(getBlock(bx, by), "Block (" + bx + ", " + by + ")"));
      }
    }

    ArrayList<Blank> blanks = new ArrayList<>();
    // Now each blank's candidate is the set intersection of the candidates of its row, column
    // and 3x3 block.
    for (int y = 0; y < 9; ++y) {
      for (int x = 0; x < 9; ++x) {
        // Only extract candidate digits for the original blank.
        if (value[y][x] == null) {
          blanks.add(new Blank(
              x, y, rowCandidates.get(y), columnCandidates.get(x),
              blockCandidates.get(y / 3 * 3 + x / 3)));
        }
      }
    }
    return blanks;
  }

  // Constructs the column array given the column index.
  private Integer[] getColumn(int x) {
    return new Integer[] {
      value[0][x], value[1][x], value[2][x],
      value[3][x], value[4][x], value[5][x],
      value[6][x], value[7][x], value[8][x],
    };
  }

  // Constructs the linearized 3x3 block array given the 3x3 block's coordinate.
  private Integer[] getBlock(int bx, int by) {
    int x = 3 * bx;
    int y = 3 * by;
    return new Integer[] {
      value[y + 0][x + 0], value[y + 0][x + 1], value[y + 0][x + 2],
      value[y + 1][x + 0], value[y + 1][x + 1], value[y + 1][x + 2],
      value[y + 2][x + 0], value[y + 2][x + 1], value[y + 2][x + 2],
    };
  }

  // For an Integer array of size 9, checks if the existing digits are valid for the puzzle.
  // Returns the set of unused digits in this group.
  private static Set<Integer> extractUnused(Integer[] nineInts, String prefix) {
    boolean[] seen = new boolean[9];
    for (Integer n : nineInts) {
      if (n == null) {
        // Nulls are missing digits.
        continue;
      }
      if (n < 1 || n > 9) {
        throw new IllegalArgumentException(prefix + " has invalid integer " + n);
      }
      // Remember to offset the index by 1!!
      if (seen[n - 1]) {
        throw new IllegalArgumentException(prefix + " has duplicated integer " + n);
      }
      seen[n - 1] = true;
    }
    HashSet<Integer> candidates = new HashSet<>();
    for (int i = 0; i < 9; ++i) {
      if (!seen[i]) {
        candidates.add(i + 1);
      }
    }
    return candidates;
  }

  // Verifies that the current state represents a valid solution, otherwise throws
  // {@link RuntimeException} to indicate a bug of the algorithm (ideally should never happen).
  private void verifySolution() {
    for (int y = 0; y < 9; ++y) {
      verifyOneToNine(value[y], "Row #" + y);
    }
    for (int x = 0; x < 9; ++x) {
      verifyOneToNine(getColumn(x), "Column #" + x);
    }
    for (int by = 0; by < 3; ++by) {
      for (int bx = 0; bx < 3; ++bx) {
        verifyOneToNine(getBlock(bx, by), "Block (" + bx + ", " + by + ")");
      }
    }
  }

  // Verifies the Integer array of size 9 consists of a permutation of digit 1-9.
  private void verifyOneToNine(Integer[] nineInts, String prefix) {
    boolean[] seen = new boolean[9];
    for (Integer n : nineInts) {
      if (n == null) {
        throw new IllegalStateException(prefix + " has null integer");
      }
      if (n < 1 || n > 9) {
        throw new IllegalStateException(prefix + " has invalid integer " + n);
      }
      // 'seen' is 0-based, remember to offset by 1.
      if (seen[n - 1]) {
        throw new IllegalStateException(prefix + " has duplicated integer " + n);
      }
      seen[n - 1] = true;
    }
  }
}
